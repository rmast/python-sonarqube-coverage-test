from src.to_case import *

def test_capital_case():
    assert capital_case('semaphore') == 'Semaphore'

def test_upper_case():
    assert upper_case('semaphore') == 'SEMAPHORE'
