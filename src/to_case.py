def capital_case(x):
    return x.capitalize()

def upper_case(x):
    return x.upper()

def lower_case(x):
    return x.lower()
